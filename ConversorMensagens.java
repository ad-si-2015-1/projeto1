import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * @author Guthierrez
 * 
 * Classe para converter qualquer objeto serializ�vel (Implemente a interface Serializable de java.io) em
 * um array de bytes e vice-versa. Uma vez convertidos esse objetos podem ser transferidos via pacotes UDP,
 * por exemplo, e depois reconstru�dos no destino.
 * 
 * 
 */
public class ConversorMensagens {
	
	/**
	 * M�todo para converter um objeto serializ�vel em um array de bytes.
	 * @param Qualquer objeto seri�lizavel
	 * @return Um array de bytes contendo o objeto
	 * 
	 * byte[] dados = new byte[1024];
     * dados = ConversorMensagens.mensagemToBytes(objeto);
	 * 
	 */
    public static byte[] mensagemToBytes(Object obj) throws IOException {
        ByteArrayOutputStream array = new ByteArrayOutputStream();
        ObjectOutputStream object = new ObjectOutputStream(array);
        object.writeObject(obj);
        return array.toByteArray();
    }
    
    /**
     * M�todo para converter um array de bytes gerado pelo m�dodo mensagemToBytes em um objeto novamente.
     * @param Um array de bytes de um objeto serializ�vel.
     * @return Um Object gen�rico que poder� ser convertido ao objeto original.
     * Exemplo: Um objeto do tipo mensagem
     * 
     * Mensagem mensagem = (Mensagem) ConversorMensagens.mensagemFromBytes(dados);
     * 
     */
    public static Object mensagemFromBytes(byte[] bytes) throws IOException, ClassNotFoundException {
        ByteArrayInputStream array = new ByteArrayInputStream(bytes);
        ObjectInputStream object = new ObjectInputStream(array);
        return object.readObject();
    }
}