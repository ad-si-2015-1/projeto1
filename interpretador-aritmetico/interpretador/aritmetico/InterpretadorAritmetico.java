package interpretador.aritmetico;

import interpretador.aritmetico.exception.CalculoImpossivelException;
import interpretador.aritmetico.exception.FormatoInvalidoException;
import interpretador.aritmetico.exception.OperacaoDesconhecidaException;

import java.math.BigDecimal;
import java.util.ArrayList;

public class InterpretadorAritmetico {
	
	private static String caracterControle;

	public static TipoOperacao parseTipoOperacao(String operacao) throws OperacaoDesconhecidaException {
		try {
			return TipoOperacao.getBySignal(operacao);
		} catch (Exception e) {
			throw new OperacaoDesconhecidaException(operacao + " não é conhecida");
		}
	}
	
	public static BigDecimal calcular(Operacao operacao) {
		return operacao.calcular();
	}
	
	public static String calcularString(String desafio) {
		Operacao oper = montarOperacao(desafio);
		try {
			BigDecimal result = calcular(oper);
			return result.toString();
		} catch (Exception e) {
			throw new CalculoImpossivelException(desafio + " impossivel de calcular", e);
		}
	}
	
	public static Double calcularDouble(String desafio) {
		Operacao oper = montarOperacao(desafio);
		try {
			BigDecimal result = calcular(oper);
			return result.doubleValue();
		} catch (Exception e) {
			throw new CalculoImpossivelException(desafio + " impossivel de calcular", e);
		}
	}
	
	public static Operacao montarOperacao(String desafio) {
		if (desafio == null || desafio.length() < 3) 
			throw new FormatoInvalidoException("\"" + desafio + "\" não possui um formato valido");
		
		ArrayList<String> sinaisConhecidos = TipoOperacao.getKnownSignals();
		for (String sinal: sinaisConhecidos) {
			if(desafio.contains(sinal)){
				TipoOperacao tipoOper = TipoOperacao.getBySignal(sinal);
				caracterControle = "|";
				String desafioSaneado = desafio.trim().replace(caracterControle, "").replace(" ", "").replace(sinal, caracterControle);
				String[] componentes = desafioSaneado.split("["+caracterControle+"]");
				
				if (componentes.length != 2) throw new FormatoInvalidoException("\"" + desafio + "\" não possui um formato valido");
				return new Operacao(tipoOper, componentes[0], componentes[1]);
			}
		}
		throw new FormatoInvalidoException("\"" + desafio + "\" não possui um formato valido");
	}
	
}
