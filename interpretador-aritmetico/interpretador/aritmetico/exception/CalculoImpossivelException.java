package interpretador.aritmetico.exception;

public class CalculoImpossivelException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public CalculoImpossivelException(String message, Throwable cause) {
        super(message, cause);
    }
	
	public CalculoImpossivelException() {
		super();
	}

	public CalculoImpossivelException(String string) {
		super(string);
	}

	public String getLocalizedMessage(){
		return "O formato da operação não é valido";
	}

}
