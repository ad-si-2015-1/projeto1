package interpretador.aritmetico.exception;

public class FormatoInvalidoException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public FormatoInvalidoException(String message, Throwable cause) {
        super(message, cause);
    }
	
	public FormatoInvalidoException() {
		super();
	}

	public FormatoInvalidoException(String string) {
		super(string);
	}

	public String getLocalizedMessage(){
		return "O formato da operação não é valido";
	}

}
