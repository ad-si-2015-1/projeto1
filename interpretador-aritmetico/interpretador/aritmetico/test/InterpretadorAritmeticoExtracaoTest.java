package interpretador.aritmetico.test;

import static interpretador.aritmetico.InterpretadorAritmetico.montarOperacao;
import static interpretador.aritmetico.InterpretadorAritmetico.parseTipoOperacao;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import interpretador.aritmetico.Operacao;
import interpretador.aritmetico.TipoOperacao;
import interpretador.aritmetico.exception.FormatoInvalidoException;
import interpretador.aritmetico.exception.OperacaoDesconhecidaException;

import org.junit.Test;

public class InterpretadorAritmeticoExtracaoTest {

	@Test
	public void parseOperationSuccessTest() {
		assertEquals(TipoOperacao.SOMA, parseTipoOperacao("+"));
		assertEquals(TipoOperacao.SUB, parseTipoOperacao("-"));
		assertEquals(TipoOperacao.MULT, parseTipoOperacao("*"));
		assertEquals(TipoOperacao.DIV, parseTipoOperacao("/"));
		assertEquals(TipoOperacao.EXP, parseTipoOperacao("^"));
		assertEquals(TipoOperacao.RAIZN, parseTipoOperacao("#"));
	}
	
	@Test(expected=OperacaoDesconhecidaException.class)
	public void parseOperationFailTest() {
		parseTipoOperacao("!");
		parseTipoOperacao("%");
		parseTipoOperacao("=");
	}
	
	@Test
	public void montarOperacaoSuccessTest() {
		Operacao operSoma = new Operacao(TipoOperacao.SOMA, "2", "4");
		assertEquals(operSoma, montarOperacao("2+4"));
		assertEquals(operSoma, montarOperacao("2+ 4"));
		assertEquals(operSoma, montarOperacao("2+4 "));
		assertNotEquals(operSoma, montarOperacao("2+44 "));
		
		Operacao operSub = new Operacao(TipoOperacao.SUB, "2", "4");
		assertEquals(operSub, montarOperacao("2-4"));
		assertEquals(operSub, montarOperacao("2- 4"));
		assertEquals(operSub, montarOperacao("2-4 "));
		assertNotEquals(operSub, montarOperacao("2-44 "));
		
		Operacao operMult = new Operacao(TipoOperacao.MULT, "2", "4");
		assertEquals(operMult, montarOperacao("2*4"));
		assertEquals(operMult, montarOperacao("2* 4"));
		assertEquals(operMult, montarOperacao("2*4 "));
		assertNotEquals(operMult, montarOperacao("2*44 "));
		
		Operacao operDiv = new Operacao(TipoOperacao.DIV, "2", "4");
		assertEquals(operDiv, montarOperacao("2/4"));
		assertEquals(operDiv, montarOperacao("2/ 4"));
		assertEquals(operDiv, montarOperacao("2/4 "));
		assertNotEquals(operDiv, montarOperacao("2/44 "));
		
		Operacao operExp = new Operacao(TipoOperacao.EXP, "2", "4");
		assertEquals(operExp, montarOperacao("2^4"));
		assertEquals(operExp, montarOperacao("2^ 4"));
		assertEquals(operExp, montarOperacao("2^4 "));
		assertNotEquals(operExp, montarOperacao("2^5 "));
		
		Operacao operRaizN = new Operacao(TipoOperacao.RAIZN, "2", "16");
		assertEquals(operRaizN, montarOperacao("2#16"));
		assertEquals(operRaizN, montarOperacao("2# 16"));
		assertEquals(operRaizN, montarOperacao("2#16 "));
		assertNotEquals(operRaizN, montarOperacao("2#17 "));

		Operacao operDeci = new Operacao(TipoOperacao.SOMA, "2.1", "4.3");
		assertEquals(operDeci, montarOperacao("2.1+4.3"));
		assertEquals(operDeci, montarOperacao("2.1+ 4.3"));
		assertEquals(operDeci, montarOperacao("2.1+4.3 "));
		assertNotEquals(operDeci, montarOperacao("2.1+4.33 "));

	}
	
	@Test(expected=FormatoInvalidoException.class)
	public void montarOperacaoFailTest() {
		montarOperacao("4.3");
	}
	

}
