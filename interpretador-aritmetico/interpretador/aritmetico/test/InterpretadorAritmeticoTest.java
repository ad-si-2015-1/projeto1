package interpretador.aritmetico.test;
import static interpretador.aritmetico.InterpretadorAritmetico.calcularDouble;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;


public class InterpretadorAritmeticoTest {
	
	@Test
	public void somaTest() {
		assertEquals(new Double(10.0) ,calcularDouble("5+5"));
		assertEquals(new Double(10.0), calcularDouble("05+5"));
		assertEquals(new Double(65.0), calcularDouble("15+50"));
		assertEquals(new Double(60000000000000015l ), calcularDouble("60000000000000000+15"));
		assertEquals(new Double(10.3) ,calcularDouble("5.1+5.2"));

		assertNotEquals(new Double(10), calcularDouble("6+5"));
		assertNotEquals(new Double(20), calcularDouble("18+5"));
		assertNotEquals(new Double(6000000000000015l), calcularDouble("60000000000000000+15"));
	}
	
	@Test
	public void subtracaoTest(){
		assertEquals(new Double(10.0) ,calcularDouble("15-5"));
		assertEquals(new Double(0), calcularDouble("5-5"));
		assertEquals(new Double(-10), calcularDouble("10-20"));
		assertEquals(new Double(0.1) ,calcularDouble("5.2-5.1"));

		assertNotEquals(new Double(2), calcularDouble("6-5"));
	}
	
	@Test
	public void multiplicacaoTest(){
		assertEquals(new Double(25) ,calcularDouble("5*5"));
		assertEquals(new Double(30.0), calcularDouble("05*6"));
		assertEquals(new Double(900000000000000000l ), calcularDouble("60000000000000000*15"));

		assertNotEquals(new Double(30.00001), calcularDouble("6*5"));
		assertNotEquals(new Double(40.00000002), calcularDouble("8*5"));
	}
	@Test
	public void divisaoTest() {
		assertEquals(new Double(25) ,calcularDouble("100/4"));
		assertEquals(new Double(30.0), calcularDouble("300/10"));
		assertEquals(new Double(60000000000000000l), calcularDouble("900000000000000000/15"));

		assertNotEquals(new Double(100), calcularDouble("1001/10"));
	}
	
	@Test
	public void exponenciacaoTest(){
		assertEquals(new Double(8) ,calcularDouble("2^3"));
		assertEquals(new Double(125), calcularDouble("5^3"));
	}
	
	@Test
	public void raizNTest(){
		assertEquals(new Double(2) ,calcularDouble("8#3"));
		assertEquals(new Double(4), calcularDouble("16#2"));
	}

}
