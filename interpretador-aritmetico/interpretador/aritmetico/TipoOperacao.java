package interpretador.aritmetico;

import static interpretador.aritmetico.MathExecutor.defaultMethodParams;
import interpretador.aritmetico.exception.OperacaoDesconhecidaException;

import java.lang.reflect.Method;
import java.util.ArrayList;

public enum TipoOperacao {
	SOMA ("+", getExecutor("somar")){
		@Override
		public String toString() {
			return "Soma";
		}
	}, 
	SUB("-", getExecutor("subtrair")){
		@Override
		public String toString() {
			return "Subtração";
		}
	}, 
	MULT("*", getExecutor("multiplicar")){
		@Override
		public String toString() {
			return "Multiplicação";
		}
	}, 
	DIV("/", getExecutor("dividir")){
		@Override
		public String toString() {
			return "Divisão";
		}
	}, 
	EXP("^", getExecutor("exponenciar")){
		@Override
		public String toString() {
			return "Exponenciação";
		}
	},
	RAIZN("#", getExecutor("raizNdeX")){
		@Override
		public String toString() {
			return "Raiz \"n\" de \"x\"";
		}
		
	};
	
	private String signal;
	private Method executor;
	private static ArrayList<String> knownSignals = new ArrayList<>();
	
	TipoOperacao(String signal, Method executor) {
		this.signal = signal;
		this.executor = executor;
	}
	
	private static Method getExecutor(String execName) {
		try {
			return MathExecutor.class.getMethod(execName, defaultMethodParams);
		} catch (Exception e) {
		}
		return null;
	}

	public String getSignal(){
		return signal;
	}
	
	public Method getExecutor(){
		return executor;
	}

	public static TipoOperacao getBySignal(String signal) {
		for (TipoOperacao value: values()) {
			if (value.signal.equalsIgnoreCase(signal)){
				return value;
			}
		}
		throw new OperacaoDesconhecidaException();
	}
	
	public static ArrayList<String> getKnownSignals() {
		if (knownSignals.size() > 0) return knownSignals;
		
		for (TipoOperacao value: values()) {
			knownSignals.add(value.signal);
		}
		return knownSignals;
	}
}
