// package br.ufg.inf.es.rcsd.lab1;

import java.io.*;
import java.net.*;
import java.util.*;

class Questionador {
	public static void main(String args[]) throws Exception {

		int porta = 9876;
		int numConn = 1;
		
		DatagramSocket serverSocket = new DatagramSocket(porta);

		byte[] receiveData = new byte[1024];
		byte[] sendData = new byte[1024];

		while (true) {

			DatagramPacket receivePacket = new DatagramPacket(receiveData,
					receiveData.length);
			System.out.println("Esperando por datagrama UDP na porta " + porta);
			serverSocket.receive(receivePacket);
			System.out.print("Datagrama UDP [" + numConn + "] recebido...");

			// vou utilizar o método trim para remover os espaços
			// em branco após a String.
			String sentence = new String(receivePacket.getData()).trim();
			System.out.println(sentence);
			String retorno = new String();
			// se o respondedor solicitar uma pergunta
			// caso o trim não tivesse sido utilizado a comparação abaixo
			// não funcionaria :-|
			if ( sentence.equals("Pergunta?") ) {
				// vamos montar um desafio aritmético
				// O StringBuffer é mais rápido do que o String
				StringBuffer sbRetorno = new StringBuffer();
				// a classe Random é do pacote java.util
				// pode ser utilizada para gerar números randomicos.
				Random gerador = new Random();
				// gerando e adicionando um número randomico de 0 a 100:
				sbRetorno.append(gerador.nextInt(100));
				sbRetorno.append("+");
				sbRetorno.append(gerador.nextInt(100));	
				sbRetorno.append("?");
				System.out.println("sbRetorno="+sbRetorno);
				// geração da string do tipo: num1+num2?
				retorno = new String(sbRetorno);
			}
			
			InetAddress IPAddress = receivePacket.getAddress();

			int port = receivePacket.getPort();

			// String capitalizedSentence = sentence.toUpperCase();
			// não iremos mais devolver uma frase em maisuculas ;-)

			// sendData = capitalizedSentence.getBytes();
			// a informação a ser retornada é o desafio gerado.
			sendData = retorno.getBytes();

			DatagramPacket sendPacket = new DatagramPacket(sendData,
					sendData.length, IPAddress, port);
			
			System.out.print("Enviando " + retorno + "...");

			serverSocket.send(sendPacket);
			System.out.println("OK\n");
		}
	}
}
