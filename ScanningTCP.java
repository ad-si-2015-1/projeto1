import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class ScanningTCP {
	static public void main(String rede[]){
		
		//Usuário digita o endereço Host onde as portas TCP serão verificadas
		System.out.println("Digite o endereço Host:");
		Scanner input = new Scanner(System.in);  
		String endereco = input.nextLine(); 
		
		//Criação do objeto para armazenar as informações de verificação de portas
		StringBuffer portasEncontradas = new StringBuffer();
		
		for(int porta=1; porta <=200; porta++){
			//Se a concexão for aberta ser problemas, a prota está aberta no Host.
			try{
				//criação do socket
				Socket s = new Socket(endereco, porta);
				//adiciona a porta na lista de portas encontradas
				portasEncontradas.append(porta+"\n");
				//socket encerrado
				s.close();
			}
			//Exceção lançada ao digitar um endereço de forma errada ou não encontrado
			catch(UnknownHostException ur){
				System.out.println("O endereço não é válido");
			}
			//Exceção lançada ao não conseguir abrir uma conexão
			catch (Exception exc){
			}
			
		}
		System.out.println("\nPortas encontradas" + portasEncontradas.toString());
	}
}