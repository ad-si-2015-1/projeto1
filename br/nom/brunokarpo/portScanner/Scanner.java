package br.nom.brunokarpo.portScanner;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class Scanner {
	static String ip;
	static int portMax = 65536;

	public static void main(String args[]) {
		try {
			ipScan();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}

	private static void ipScan() throws UnknownHostException {
		StringBuilder sb;

		for(int i = 1; i < 254; i++) {
			sb = new StringBuilder();
			sb.append("192.168.1.");
			sb.append(String.valueOf(i));

			ip = sb.toString();

			System.out.println("====== SCANEANDO PORTAS DO HOST " + ip + " ======");
			portScan();
		}
	}

	private static void portScan() {

		System.out.println("Scaneandoas, por favor aguarde" + "\n");

		try {

			InetAddress addr = InetAddress.getByName(ip);
			String hostname = addr.getHostName();

			for (int i = 0; i < portMax; i++) {
				Socket s = null;

				try {
					s = new Socket(addr, i);
					System.out.println("A porta " + i + " está aberta em: "
							+ hostname);
				} catch (IOException ex) {
				} finally {
					try {
						// Fecha o socket
						if (s != null)
							s.close();
					} catch (IOException ex) {
					}
				}
			}
		} catch (UnknownHostException ex) {
			System.err.println(ex);
		}

		System.out.println("\nScannerlizado.\n");
	}
}