/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transferencia;

import java.io.*;
import java.net.*;

/**
 *
 * @author Miriã
 */
public class Servidor {
    public static void main(String[] args) throws IOException{
        //Cria o socket.
        ServerSocket servsock = new ServerSocket(123456);
        //File myFile = new File("TesteEnvioArquivo.txt");
        
        while (true) {
            Socket sock = servsock.accept();
            System.out.println("Conexão Aceita: " + sock);
            
            //Envia o arquivo (transforma em byte array)
            File myFile = new File ("TesteEnvioArquivo.txt");
            byte [] mybytearray = new byte[(int)myFile.length()];
            FileInputStream fis = new FileInputStream(myFile);
            BufferedInputStream bis = new BufferedInputStream(fis);
            //BufferedInputStream bis = new BufferedInputStream(new FileInputStream(myFile));
            bis.read(mybytearray, 0, mybytearray.length);
            OutputStream os = sock.getOutputStream();
            System.out.println("Enviando...");
            os.write(mybytearray, 0, mybytearray.length);
            os.flush();
            sock.close();
        }
    }
}